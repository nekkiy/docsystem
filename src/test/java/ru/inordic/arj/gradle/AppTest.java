package ru.inordic.arj.gradle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.TestSecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.inordic.arj.gradle.model.Document;
import ru.inordic.arj.gradle.service.DocumentService;
import ru.inordic.arj.gradle.service.OperatorStorage;
import ru.inordic.arj.gradle.web.dto.DocumentView;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class AppTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestUtils testUtils;

    @Autowired
    private OperatorStorage operatorStorage;
    @Autowired
    private DocumentService documentService;


    @BeforeEach
    void setup() {
        testUtils.clearAllDocs();
    }


    @Test
    void test() throws Exception {
        UserDetails inspector = operatorStorage.loadUserByUsername("inspector");
        UserDetails expert = operatorStorage.loadUserByUsername("expert");
        UsernamePasswordAuthenticationToken inspectorAt =
                new UsernamePasswordAuthenticationToken(inspector, inspector.getPassword(), inspector.getAuthorities());
        UsernamePasswordAuthenticationToken expertAt =
                new UsernamePasswordAuthenticationToken(expert, expert.getPassword(), expert.getAuthorities());
        try {
            {
                TestSecurityContextHolder.setAuthentication(inspectorAt);
                List<DocumentView> docs = documentService.getDocViewsForCurrentOper();
                Assertions.assertEquals(0, docs.size());
            }
            TestSecurityContextHolder.setAuthentication(inspectorAt);
            mockMvc.perform(MockMvcRequestBuilders.post("/createDocument.html")
                    .param("inn", "123")
                    .param("orgName", "test")
                    .param("address", "test"))
                    .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
            int docId;
            {
                TestSecurityContextHolder.setAuthentication(inspectorAt);
                List<DocumentView> docs = documentService.getDocViewsForCurrentOper();
                Assertions.assertEquals(1, docs.size());
                DocumentView doc = docs.get(0);
                Assertions.assertEquals(Document.Status.CREATED, doc.getStatus());
                docId = doc.getId();
            }

            {
                TestSecurityContextHolder.setAuthentication(expertAt);
                List<DocumentView> docs = documentService.getDocViewsForCurrentOper();
                Assertions.assertEquals(1, docs.size());
            }

            TestSecurityContextHolder.setAuthentication(expertAt);
            mockMvc.perform(MockMvcRequestBuilders.post("/approveDoc.html")
                    .param("id", "" + docId)
                    .param("licenseNumber", "test"))
                    .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

            {
                TestSecurityContextHolder.setAuthentication(expertAt);
                List<DocumentView> docs = documentService.getDocViewsForCurrentOper();
                Assertions.assertEquals(1, docs.size());
                DocumentView doc = docs.get(0);
                Assertions.assertEquals(Document.Status.APPROVED, doc.getStatus());
            }

            {
                TestSecurityContextHolder.setAuthentication(inspectorAt);
                List<DocumentView> docs = documentService.getDocViewsForCurrentOper();
                Assertions.assertEquals(1, docs.size());
                DocumentView doc = docs.get(0);
                Assertions.assertEquals(Document.Status.APPROVED, doc.getStatus());
            }
        } finally {
            TestSecurityContextHolder.clearContext();
        }
    }

}