package ru.inordic.arj.gradle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Map;

/**
 * @author Artem R. Romanenko
 * @version 23.02.2021
 */
@Component
public class TestUtils {


    @Autowired
    private NamedParameterJdbcOperations npjo;

    @Transactional
    public void clearAllDocs() {
        npjo.update("delete from app.document", Map.of());
    }
}
