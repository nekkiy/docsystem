package ru.inordic.arj.gradle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.dao.OperatorDao;
import ru.inordic.arj.gradle.model.Operator;
import ru.inordic.arj.gradle.model.Role;
import ru.inordic.arj.gradle.util.OperUdImpl;
import ru.inordic.arj.gradle.util.WebException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class OperatorStorageImpl implements OperatorStorage {

    @Autowired
    private OperatorDao dao;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Operator op = dao.getByLogin(username);
              return new OperUdImpl(op.getId(), op.getLogin(), Role.valueOf(op.getRole()), op.getPassword(), op.isDisable());
    }

    @Override
    @Transactional
    public void createOperator(String login, String password, String fio, String role) {
        boolean isExist = dao.existByLogin(login);
        if (isExist) {
            throw new WebException("User with login " + login + " already exists");
        }
        dao.createNewOperator(login, password, fio, role);

    }

    @Override
    public List<Operator> getOperators() {
        return dao.getAllOrderByDisableAndLogin();
    }

    @Override
    public void lockOperatorById(int id) {
        Operator op = dao.getById(id);
        if (op.getRole().equals("ADMIN")) {
            throw new WebException("Нельзя заблокировать пользователя с ролью ADMIN");
        }
        if (op.isDisable()) {
            throw new WebException("Пользователь уже заблокирован");
        }
        dao.setDisableById(id, true);


    }
}
