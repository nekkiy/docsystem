package ru.inordic.arj.gradle.service;

import ru.inordic.arj.gradle.web.dto.DocumentView;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public interface DocumentService {

    List<DocumentView> getDocViewsForCurrentOper();

    String createNewDocument(String inn, String orgName, String address);

    DocumentView getDocumentView(int id);

    void approveDoc(int id, String licenseNumber);

    void declineDoc(int id, String reason);
}
