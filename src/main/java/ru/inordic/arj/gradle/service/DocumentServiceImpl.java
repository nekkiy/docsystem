package ru.inordic.arj.gradle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.dao.DocumentDao;
import ru.inordic.arj.gradle.model.Document;
import ru.inordic.arj.gradle.model.Role;
import ru.inordic.arj.gradle.util.OperUd;
import ru.inordic.arj.gradle.util.SecurityUtils;
import ru.inordic.arj.gradle.web.dto.DocumentView;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
@Component
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentDao dao;


    @Override
    public List<DocumentView> getDocViewsForCurrentOper() {
        OperUd ud = SecurityUtils.currentUserDetails();
        switch (ud.getRole()) {
            case ADMIN:
                return Collections.emptyList();
            case INSPECTOR:
                return dao.getDocumentViewsByInspector(ud.getId());
            case EXPERT:
                List<DocumentView> l1 = dao.getDocumentViewsByStatus(Document.Status.CREATED);
                List<DocumentView> l2 = dao.getDocumentViewsByExpert(ud.getId());
                List<DocumentView> result = new ArrayList<>(l1);
                result.addAll(l2);
                return result;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    @Transactional
    public String createNewDocument(String inn, String orgName, String address) {
        OperUd ud = SecurityUtils.currentUserDetails();
        String income = dao.generateIncome();
        Document doc = new Document();
        doc.setIncome(income);
        doc.setInn(inn);
        doc.setOrgName(orgName);
        doc.setAddress(address);
        doc.setStatus(Document.Status.CREATED);
        doc.setInspectorId(ud.getId());
        doc.setCreateTs(OffsetDateTime.now());
        dao.save(doc);
        return income;
    }

    @Override
    public DocumentView getDocumentView(int id) {
        return dao.getDocumentView(id);
    }

    @Override
    @Transactional
    public void approveDoc(int id, String licenseNumber) {
        OperUd ud = SecurityUtils.currentUserDetails();
        if (ud.getRole() != Role.EXPERT) {
            throw new IllegalStateException();
        }
        Document doc = dao.get(id);
        doc.setStatus(Document.Status.APPROVED);
        doc.setEndTs(OffsetDateTime.now());
        doc.setExpertId(ud.getId());
        doc.setLicenceNumber(licenseNumber);
        dao.save(doc);
    }

    @Override
    public void declineDoc(int id, String reason) {
        OperUd ud = SecurityUtils.currentUserDetails();
        if (ud.getRole() != Role.EXPERT) {
            throw new IllegalStateException();
        }
        Document doc = dao.get(id);
        doc.setStatus(Document.Status.DECLINED);
        doc.setEndTs(OffsetDateTime.now());
        doc.setExpertId(ud.getId());
        doc.setDeclineReason(reason);
        dao.save(doc);
    }
}
