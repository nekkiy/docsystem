package ru.inordic.arj.gradle.service;

import ru.inordic.arj.gradle.model.Operator;

import java.util.Map;
import java.util.UUID;

public interface SessionStorage {
    String NRD_SESSION = "NRD_SESSION";

    void save(UUID sessionId, Operator operator);
}
