package ru.inordic.arj.gradle.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.inordic.arj.gradle.model.Operator;

import java.util.List;

public interface OperatorStorage extends UserDetailsService {


    void createOperator(String login, String password, String fio, String role);

    List<Operator> getOperators();

    void lockOperatorById(int id);
}
