package ru.inordic.arj.gradle.model;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public enum Role {
    ADMIN, EXPERT, INSPECTOR
}
