package ru.inordic.arj.gradle.model;

import java.time.OffsetDateTime;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public class Document {

    private int id;
    private String income;
    private String inn;
    private String orgName;
    private String address;
    private int inspectorId;
    private OffsetDateTime createTs;
    private Integer expertId;
    private OffsetDateTime endTs;
    private Status status;
    private String declineReason;
    private String licenceNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getInspectorId() {
        return inspectorId;
    }

    public void setInspectorId(int inspectorId) {
        this.inspectorId = inspectorId;
    }

    public OffsetDateTime getCreateTs() {
        return createTs;
    }

    public void setCreateTs(OffsetDateTime createTs) {
        this.createTs = createTs;
    }

    public Integer getExpertId() {
        return expertId;
    }

    public void setExpertId(Integer expertId) {
        this.expertId = expertId;
    }

    public OffsetDateTime getEndTs() {
        return endTs;
    }

    public void setEndTs(OffsetDateTime endTs) {
        this.endTs = endTs;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public enum Status {
        CREATED, APPROVED, DECLINED
    }
}
