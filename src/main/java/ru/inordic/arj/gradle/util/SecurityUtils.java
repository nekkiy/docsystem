package ru.inordic.arj.gradle.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import ru.inordic.arj.gradle.model.Role;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public class SecurityUtils {

    private SecurityUtils() {

    }

    public static OperUd currentUserDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken up = (UsernamePasswordAuthenticationToken) auth;
            return (OperUd) up.getPrincipal();
        } else {
            return null;
        }

    }
}
