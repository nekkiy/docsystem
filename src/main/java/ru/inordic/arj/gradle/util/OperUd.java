package ru.inordic.arj.gradle.util;

import ru.inordic.arj.gradle.model.Role;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public interface OperUd {
    Role getRole();

    int getId();

    String getUsername();
}
