package ru.inordic.arj.gradle.util;

/**
 * @author Artem R. Romanenko
 * @version 20.02.2021
 */
public class WebException extends RuntimeException{
    public WebException(String message) {
        super(message);
    }

    public WebException(String message, Throwable cause) {
        super(message, cause);
    }
}
