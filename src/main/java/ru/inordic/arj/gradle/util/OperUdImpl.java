package ru.inordic.arj.gradle.util;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.inordic.arj.gradle.model.Role;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author Artem R. Romanenko
 * @version 21.02.2021
 */
public class OperUdImpl implements OperUd, UserDetails, CredentialsContainer {
    private final int id;
    private final String username;
    private final Role role;
    private final Set<SimpleGrantedAuthority> authorities;
    private final boolean disable;
    private String password;


    public OperUdImpl(String username, Role role, int id) {
        this(id, username, role, null, false);
    }

    public OperUdImpl(int id, String username, Role role, String password, boolean disable) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.password = password;
        this.disable = disable;
        authorities = Set.of(new SimpleGrantedAuthority("ROLE_" + getRole()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !disable;
    }


    public Role getRole() {
        return role;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void eraseCredentials() {
        password = null;

    }
}
