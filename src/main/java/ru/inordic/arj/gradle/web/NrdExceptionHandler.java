package ru.inordic.arj.gradle.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.inordic.arj.gradle.util.WebException;

@ControllerAdvice
public class NrdExceptionHandler {

    @ExceptionHandler(WebException.class)
    public String accessDenied(WebException e, Model model) {
        System.out.println("Error " + e.getMessage());
        e.printStackTrace();
        model.addAttribute("reason", e.getMessage());
        return "accessDenied";
    }

    @ExceptionHandler(Exception.class)
    public String defaultHandle(Exception e, Model model) {
        System.out.println("Error " + e.getMessage());
        e.printStackTrace();
        model.addAttribute("reason", "Неизвестная ошибка");
        return "error";
    }
}
