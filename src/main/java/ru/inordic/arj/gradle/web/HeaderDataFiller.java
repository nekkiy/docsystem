package ru.inordic.arj.gradle.web;


import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import ru.inordic.arj.gradle.util.OperUd;
import ru.inordic.arj.gradle.util.SecurityUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Artem R. Romanenko
 * @version 19.02.2021
 */

public class HeaderDataFiller implements HandlerInterceptor {
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            OperUd ud = SecurityUtils.currentUserDetails();
            if (ud != null) {
                switch (ud.getRole()) {
                    case ADMIN:
                        modelAndView.addObject("roleAdmin", true);
                        break;
                    case EXPERT:
                        modelAndView.addObject("roleExpert", true);
                        break;
                    case INSPECTOR:
                        modelAndView.addObject("roleInspector", true);
                        break;
                }
                modelAndView.addObject("userDetails", ud);
            }
        }
    }
}
