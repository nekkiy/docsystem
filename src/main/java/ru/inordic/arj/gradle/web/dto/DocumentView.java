package ru.inordic.arj.gradle.web.dto;

import ru.inordic.arj.gradle.model.Document;

/**
 * @author Artem R. Romanenko
 * @version 22.02.2021
 */
public class DocumentView extends Document {
    private String inspectorFio;
    private String expertFio;

    public String getInspectorFio() {
        return inspectorFio;
    }

    public void setInspectorFio(String inspectorFio) {
        this.inspectorFio = inspectorFio;
    }

    public String getExpertFio() {
        return expertFio;
    }

    public void setExpertFio(String expertFio) {
        this.expertFio = expertFio;
    }
}
