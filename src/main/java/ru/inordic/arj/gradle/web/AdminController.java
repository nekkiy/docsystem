package ru.inordic.arj.gradle.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inordic.arj.gradle.model.Operator;
import ru.inordic.arj.gradle.service.OperatorStorage;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 19.02.2021
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private OperatorStorage os;

    @GetMapping("index.html")
    public String index(Model model) {
        List<Operator> users = os.getOperators();
        model.addAttribute("users", users);
        return "adminIndex";
    }

    @GetMapping("addUser.html")
    public String addUser() {
        return "addUser";
    }

    @PostMapping("addUser")
    public String addUserResult(Model model,
                                @RequestParam("login") String login, @RequestParam("password") String password,
                                @RequestParam("fio") String fio, @RequestParam("role") String role) {
        os.createOperator(login, password, fio, role);
        return "successResult";
    }

    @PostMapping("lockUser")
    public String lockUser(@RequestParam("id") int id) {
        os.lockOperatorById(id);
        return "successResult";
    }
}
