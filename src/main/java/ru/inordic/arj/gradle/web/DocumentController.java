package ru.inordic.arj.gradle.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inordic.arj.gradle.model.Document;
import ru.inordic.arj.gradle.service.DocumentService;
import ru.inordic.arj.gradle.util.OperUd;
import ru.inordic.arj.gradle.util.SecurityUtils;
import ru.inordic.arj.gradle.web.dto.DocumentView;

import java.util.List;

@Controller
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping(value = "/index.html", produces = "text/html")
    public String indexHtml(Model model) throws Exception {
        List<DocumentView> docViews = documentService.getDocViewsForCurrentOper();
        model.addAttribute("docs", docViews);
        return "index";
    }


    @GetMapping(value = "/createDocument.html", produces = "text/html")
    @Secured("ROLE_INSPECTOR")
    public String createDocument() throws Exception {
        return "createDocument";
    }


    @PostMapping(value = "/createDocument.html", produces = "text/html")
    @Secured("ROLE_INSPECTOR")
    public String createUserPost(@RequestParam("inn") String inn,
                                 @RequestParam("orgName") String orgName,
                                 @RequestParam("address") String address,
                                 Model model) throws Exception {
        String income = documentService.createNewDocument(inn, orgName, address);
        model.addAttribute("customMessage",
                "Документ создан успешно. Входящий номер '" + income + "'");
        return "successResult";
    }

    @PostMapping(value = "/approveDoc.html", produces = "text/html")
    @Secured("ROLE_EXPERT")
    public String approveDoc(@RequestParam("id") int id,
                             @RequestParam("licenseNumber") String licenseNumber,
                             Model model) throws Exception {
        documentService.approveDoc(id, licenseNumber);
        model.addAttribute("customMessage",
                "Лицензия успешно выдана.");
        return "successResult";
    }

    @PostMapping(value = "/declineDoc.html", produces = "text/html")
    @Secured("ROLE_EXPERT")
    public String declineDoc(@RequestParam("id") int id,
                             @RequestParam("reason") String reason,
                             Model model) throws Exception {
        documentService.declineDoc(id, reason);
        model.addAttribute("customMessage",
                "Лицензия успешно выдана.");
        return "successResult";
    }


    @GetMapping(value = "/doc/{id}", produces = "text/html")
    public String getDoc(@PathVariable("id") int id, Model model) throws Exception {
        DocumentView docView = documentService.getDocumentView(id);
        model.addAttribute("doc", docView);
        OperUd ud = SecurityUtils.currentUserDetails();
        switch (ud.getRole()) {
            case INSPECTOR:
                break;
            case EXPERT:
                if (docView.getStatus() == Document.Status.CREATED) {
                    model.addAttribute("allowExpertOperation", true);
                }
                break;
        }
        return "document";
    }
}
