package ru.inordic.arj.gradle.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inordic.arj.gradle.model.Operator;
import ru.inordic.arj.gradle.service.OperatorStorage;
import ru.inordic.arj.gradle.service.SessionStorageImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Controller
public class LoginController {


    @Autowired
    private OperatorStorage operatorStorage;
//    @Autowired
//    private SessionStorage sessionStorage;

    @GetMapping("/login.html")
    public String getLoginPage(Model model) {
//        Object operator = SecurityContextHolder.getContext().getAuthentication().getDetails();
        model.addAttribute("operator", null);
        return "login";
    }

//    @PostMapping("/login")
//    public String loginoObrabativatel(@RequestParam("username") String username,
//                                      @RequestParam("password") String password,
//                                      Model model, HttpServletResponse servletResponse
//    ) {
//        boolean result = operatorStorage.verify(username, password);
//        if (result) {
//            model.addAttribute("result", "Вы успешно залогинились");
//            UUID sessionId = UUID.randomUUID();
//            Cookie sessionCookie = new Cookie(SessionStorage.NRD_SESSION, sessionId.toString());
//            Operator operator = operatorStorage.loadByUserName(username);
//            sessionStorage.save(sessionId, operator);
//
//            servletResponse.addCookie(sessionCookie);
//        } else {
//            model.addAttribute("result", "Лоин или пароль не верны");
//        }
//        return "loginResult";
//    }
}
