package ru.inordic.arj.gradle.dao;

import ru.inordic.arj.gradle.model.Operator;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 19.02.2021
 */
public interface OperatorDao {


    Operator getByLogin(String username);

    boolean existByLogin(String login);

    void createNewOperator(String login, String password, String fio, String role);

    List<Operator> getAllOrderByDisableAndLogin();

    Operator getById(int id);

    void setDisableById(int id, boolean b);
}
