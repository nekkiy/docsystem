package ru.inordic.arj.gradle.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Operator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Artem R. Romanenko
 * @version 19.02.2021
 */
@Component
public class OperatorDaoImpl implements OperatorDao {
    private final RowMapper<Operator> rm = new BeanPropertyRowMapper<>(Operator.class);

    @Autowired
    private NamedParameterJdbcOperations npjo;

    @Override
    public Operator getByLogin(String username) {
        return npjo.queryForObject("select * from app.operator where login=:login", new MapSqlParameterSource("login", username), rm);
    }

    @Override
    public Operator getById(int id) {
        return npjo.queryForObject("select * from app.operator where id=:id", new MapSqlParameterSource("id", id), rm);
    }

    @Override
    public void setDisableById(int id, boolean disable) {
        npjo.update("update app.operator set disable =:disable where id =:id ", new MapSqlParameterSource("id", id)
                .addValue("disable", disable));
    }

    @Override
    public List<Operator> getAllOrderByDisableAndLogin() {
        return npjo.query("select * from app.operator order by disable, login", new EmptySqlParameterSource(), rm);
    }

    @Override
    public boolean existByLogin(String login) {
        Integer cnt = npjo.queryForObject("select count(*) from app.operator where login=:login", new MapSqlParameterSource("login", login), Integer.class);
        return cnt > 0;
    }

    @Override
    public void createNewOperator(String login, String password, String fio, String role) {
        Map<String, Object> params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);
        params.put("fio", fio);
        params.put("role", role);
        npjo.update("insert into app.operator(id, login, password, fio, role, disable) " +
                        " values (app.seq_operator.nextval, :login, :password, :fio, :role, false);",
                params);
    }
}
