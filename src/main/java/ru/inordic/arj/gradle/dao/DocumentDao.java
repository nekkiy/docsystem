package ru.inordic.arj.gradle.dao;

import ru.inordic.arj.gradle.model.Document;
import ru.inordic.arj.gradle.web.dto.DocumentView;

import java.util.List;

/**
 * @author Artem R. Romanenko
 * @version 20.02.2021
 */
public interface DocumentDao {
    List<Document> getDocumentsByInspector(int inspectorId);

    List<DocumentView> getDocumentViewsByInspector(int inspectorId);

    String generateIncome();

    void save(Document doc);

    List<DocumentView> getDocumentViewsByExpert(int expertId);

    List<DocumentView> getDocumentViewsByStatus(Document.Status status);

    DocumentView getDocumentView(int id);

    Document get(int id);
}
