package ru.inordic.arj.gradle.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Document;
import ru.inordic.arj.gradle.web.dto.DocumentView;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Artem R. Romanenko
 * @version 20.02.2021
 */
@Component
public class DocumentDaoImpl implements DocumentDao {

    private static final String SELECT_VIEW_BASE = " select d.*, oem.fio inspectorFio, oex.fio expertFio from app.document d " +
            " left outer join app.operator oem on d.inspector_id=oem.id" +
            " left outer join app.operator oex on d.expert_id=oex.id ";

    private RowMapper<Document> rm = new BeanPropertyRowMapper<>(Document.class);
    private RowMapper<DocumentView> docViewRm = new BeanPropertyRowMapper<>(DocumentView.class);
    @Autowired
    private NamedParameterJdbcOperations npjo;

    @Override
    public Document get(int id) {
        return npjo.queryForObject("select * from app.document where id =:id ", new MapSqlParameterSource("id", id), rm);
    }

    @Override
    public List<Document> getDocumentsByInspector(int inspectorId) {
        return npjo.query("select * from app.document where inspector_id =:inspectorId order by create_ts desc", new MapSqlParameterSource("inspectorId", inspectorId), rm);
    }

    @Override
    public List<DocumentView> getDocumentViewsByInspector(int inspectorId) {
        return npjo.query(SELECT_VIEW_BASE +
                        "  where d.inspector_id =:inspectorId order by d.create_ts desc",
                new MapSqlParameterSource("inspectorId", inspectorId), docViewRm);
    }

    @Override
    public List<DocumentView> getDocumentViewsByExpert(int expertId) {
        return npjo.query(SELECT_VIEW_BASE +
                        "  where d.expert_id =:expertId order by d.create_ts desc",
                new MapSqlParameterSource("expertId", expertId), docViewRm);
    }

    @Override
    public List<DocumentView> getDocumentViewsByStatus(Document.Status status) {
        return npjo.query(SELECT_VIEW_BASE +
                        "  where d.status =:status order by d.create_ts desc",
                new MapSqlParameterSource("status", status.name()), docViewRm);
    }

    @Override
    public DocumentView getDocumentView(int id) {
        return npjo.queryForObject(SELECT_VIEW_BASE +
                        "  where d.id =:id ",
                new MapSqlParameterSource("id", id), docViewRm);
    }


    @Override
    public String generateIncome() {
        Integer tmp = npjo.queryForObject("select app.seq_income.nextval", Collections.emptyMap(), Integer.class);
        return tmp + "/AVC";
    }

    @Override
    public void save(Document doc) {
        String sql;
        if (doc.getId() == 0) {
            Integer id = npjo.queryForObject("select app.seq_document.nextval", Collections.emptyMap(), Integer.class);
            doc.setId(id);
            sql = "insert into app.document (id, income, inn, org_name, address,  inspector_id, create_ts, expert_id, end_ts, status, decline_reason, licence_number) " +
                    " values(:id, :income, :inn, :orgName, :address, :inspectorId, :createTs, :expertId, :endTs, :status, :declineReason, :licenceNumber)";
        } else {
            sql = "update app.document set income=:income, inn=:inn, org_name=:orgName," +
                    " address=:address, inspector_id=:inspectorId, create_ts=:createTs, expert_id=:expertId," +
                    " end_ts=:endTs, status=:status, decline_reason=:declineReason, licence_number=:licenceNumber " +
                    " where id=:id";
        }

        Map<String, Object> p = new HashMap<>();
        p.put("id", doc.getId());
        p.put("income", doc.getIncome());
        p.put("inn", doc.getInn());
        p.put("orgName", doc.getOrgName());
        p.put("address", doc.getAddress());
        p.put("inspectorId", doc.getInspectorId());
        p.put("createTs", doc.getCreateTs());
        p.put("expertId", doc.getExpertId());
        p.put("endTs", doc.getEndTs());
        p.put("status", doc.getStatus().name());
        p.put("declineReason", doc.getDeclineReason());
        p.put("licenceNumber", doc.getLicenceNumber());

        npjo.update(sql, p);


    }
}

