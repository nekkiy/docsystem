create schema app;

create table app.operator
(
    id       int primary key not null,
    login    varchar         not null,
    password varchar         not null,
    fio      varchar         not null,
    role     varchar         not null,
    disable  boolean         not null
);
create
unique index app.operator_ul on app.operator(login);
create sequence app.seq_operator;



create table app.document
(
    id             int primary key not null,
    income         varchar         not null,
    inn            varchar         not null,
    org_name       varchar         not null,
    address        varchar         not null,
    inspector_id    int             not null references operator,
    create_ts      timestamp       not null,
    expert_id      int references operator,
    end_ts         timestamp,
    status         varchar         not null,
    decline_reason varchar,
    licence_number varchar

);
create sequence app.seq_document;
create sequence app.seq_income nocache;